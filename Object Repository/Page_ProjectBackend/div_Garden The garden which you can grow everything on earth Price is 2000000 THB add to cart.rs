<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Garden The garden which you can grow everything on earth Price is 2000000 THB add to cart</name>
   <tag></tag>
   <elementGuidId>9d5d0917-1cb9-499e-9cb4-a1c8cd88518f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Products'])[2]/following::div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>farmer-all-card</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      
        
          

            
          
          
            Garden

            The garden which you can grow everything on earth
            
              Price is 20,000.00 THB
            
            
               add to cart
            
            
          
        
      
        
          

            
          
          
            Banana

            A good fruit with very cheap price
            
              Price is 150.00 THB
            
            
               add to cart
            
            
          
        
      
        
          

            
          
          
            Orange

            Nothing good about it
            
              Price is 280.00 THB
            
            
               add to cart
            
            
          
        
      
        
          

            
          
          
            Papaya

            Use for papaya salad
            
              Price is 12.00 THB
            
            
               add to cart
            
            
          
        
      
        
          

            
          
          
            Rambutan

            An expensive fruit from the sout
            
              Price is 20.00 THB
            
            
               add to cart
            
            
          
        
      
    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-product-list[1]/div[@class=&quot;dashboard-card&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;farmer-all-card&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Products'])[2]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/following::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div</value>
   </webElementXpaths>
</WebElementEntity>
