import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://18.206.156.168:8087/')

WebUI.waitForPageLoad(3)

WebUI.verifyElementVisible(findTestObject('Page_login/button_Login'))

WebUI.verifyElementVisible(findTestObject('Page_login/h1_SE 234 Project'))

WebUI.verifyElementVisible(findTestObject('Page_login/h2_Login'))

WebUI.verifyElementVisible(findTestObject('Page_login/input_Password_password'))

WebUI.verifyElementVisible(findTestObject('Page_login/input_Username_username'))

WebUI.verifyElementVisible(findTestObject('Page_login/label_Password'))

WebUI.verifyElementVisible(findTestObject('Page_login/label_Username'))

WebUI.verifyElementVisible(findTestObject('Page_login/p_This is the mock app for the SE 234 project'))

WebUI.closeBrowser()

