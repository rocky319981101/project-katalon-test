import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://18.206.156.168:8087/')

WebUI.waitForPageLoad(0)

WebUI.verifyElementPresent(findTestObject('Page_ProjectBackend/h2_Login'), 1)

WebUI.verifyElementVisible(findTestObject('Page_ProjectBackend/input_Username_username'))

WebUI.verifyElementVisible(findTestObject('Page_ProjectBackend/input_Password_password'))

WebUI.navigateToUrl('http://18.206.156.168:8087/otherUrl')

WebUI.verifyElementPresent(findTestObject('Page_ProjectBackend/h2_Login'), 1)

WebUI.verifyElementVisible(findTestObject('Page_ProjectBackend/input_Username_username'))

WebUI.verifyElementVisible(findTestObject('Page_ProjectBackend/input_Password_password'))

WebUI.navigateToUrl('http://18.206.156.168:8087/products')

WebUI.verifyElementPresent(findTestObject('Page_ProjectBackend/h2_Login'), 1)

WebUI.verifyElementVisible(findTestObject('Page_ProjectBackend/input_Username_username'))

WebUI.verifyElementVisible(findTestObject('Page_ProjectBackend/input_Password_password'))

WebUI.closeBrowser()

