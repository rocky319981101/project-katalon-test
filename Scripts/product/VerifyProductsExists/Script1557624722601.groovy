import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://18.206.156.168:8087/')

WebUI.waitForPageLoad(3)

WebUI.verifyElementPresent(findTestObject('Page_ProjectBackend/h2_Login'), 1)

WebUI.verifyElementVisible(findTestObject('Page_ProjectBackend/input_Username_username'))

WebUI.verifyElementVisible(findTestObject('Page_ProjectBackend/input_Password_password'))

WebUI.setText(findTestObject('Object Repository/Page_ProjectBackend/input_Username_username'), 'user')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ProjectBackend/input_Password_password'), '1/VWEm4uipk=')

WebUI.click(findTestObject('Object Repository/Page_ProjectBackend/button_Login'))

WebUI.waitForPageLoad(3)

WebUI.verifyElementVisible(findTestObject('Page_product/a_Carts'))

WebUI.verifyElementVisible(findTestObject('Page_product/a_Products'))

WebUI.verifyElementVisible(findTestObject('Page_product/button_add to cart'))

WebUI.verifyElementVisible(findTestObject('Page_product/button_Logout'))

WebUI.verifyElementVisible(findTestObject('Page_product/h1_SE 234 Project'))

WebUI.verifyElementVisible(findTestObject('Page_product/h2_Products'))

WebUI.verifyElementVisible(findTestObject('Page_product/h5_Banana'))

WebUI.verifyElementVisible(findTestObject('Page_product/h5_Garden'))

WebUI.verifyElementVisible(findTestObject('Page_product/h5_Orange'))

WebUI.verifyElementVisible(findTestObject('Page_product/h5_Papaya'))

WebUI.verifyElementVisible(findTestObject('Page_product/h5_Rambutan'))

WebUI.verifyElementVisible(findTestObject('Page_product/h6_Price is 1200 THB'))

WebUI.verifyElementVisible(findTestObject('Page_product/h6_Price is 15000 THB'))

WebUI.verifyElementVisible(findTestObject('Page_product/h6_Price is 2000 THB'))

WebUI.verifyElementVisible(findTestObject('Page_product/h6_Price is 2000000 THB'))

WebUI.verifyElementVisible(findTestObject('Page_product/h6_Price is 28000 THB'))

WebUI.verifyElementVisible(findTestObject('Page_product/img'))

WebUI.verifyElementVisible(findTestObject('Page_product/p_This is the mock app for the SE 234 project'))

WebUI.verifyElementVisible(findTestObject('Page_product/p_Use for papaya salad'))

WebUI.closeBrowser()

