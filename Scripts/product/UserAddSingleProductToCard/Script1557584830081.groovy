import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://18.206.156.168:8087/')

WebUI.waitForPageLoad(3)

WebUI.verifyElementPresent(findTestObject('Page_ProjectBackend/h2_Login'), 1)

WebUI.verifyElementVisible(findTestObject('Page_ProjectBackend/input_Username_username'))

WebUI.verifyElementVisible(findTestObject('Page_ProjectBackend/input_Password_password'))

WebUI.setText(findTestObject('Object Repository/Page_ProjectBackend/input_Username_username'), 'user')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ProjectBackend/input_Password_password'), '1/VWEm4uipk=')

WebUI.click(findTestObject('Object Repository/Page_ProjectBackend/button_Login'))

WebUI.waitForPageLoad(3)

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_ProjectBackend/a_Products'))

WebUI.verifyElementVisible(findTestObject('Page_ProjectBackend/a_Carts'))

WebUI.verifyElementVisible(findTestObject('Page_ProjectBackend/h5_Banana'))

WebUI.verifyElementVisible(findTestObject('Page_ProjectBackend/button_add to cart_banana'))

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart_banana'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_ProjectBackend/span_1'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('Page_ProjectBackend/div_banbna_already added'))

WebUI.closeBrowser()

